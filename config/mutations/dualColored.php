<?php
class DualColored
{
    var $commonality = 15;

    var $name = "Dual-Colored";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        $dragon->secondColor = $dragon->color;

        while ($dragon->secondColor == $dragon->color) //Ensures the second color is not identical to the original color.
        {
            $dragon->secondColor = RandomElement(["Green", "Blue", "Brown", "Bronze", "Gold"]);
        }

        return $dragon;
    }
}

RegisterMutation(new DualColored());