<?php
class TriColored
{
    var $commonality = 10;

    var $name = "Tri-Colored";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        $dragon->secondColor = $dragon->color;

        while ($dragon->secondColor == $dragon->color) //Ensures the second color is not identical to the original color.
        {
            $dragon->secondColor = RandomElement(["Green", "Blue", "Brown", "Bronze", "Gold"]);
        }

        $dragon->thirdColor = $dragon->color;

        while ($dragon->thirdColor == $dragon->color || $dragon->thirdColor == $dragon->secondColor)
        {
            $dragon->thirdColor = RandomElement(["Green", "Blue", "Brown", "Bronze", "Gold"]);
        }

        return $dragon;
    }
}

RegisterMutation(new TriColored());