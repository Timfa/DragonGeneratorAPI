<?php
class LeuismPiebald
{
    var $commonality = 10;

    var $name = "Leuism, Piebald";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        return $dragon;
    }
}

RegisterMutation(new LeuismPiebald());