<?php
class FullMelanism
{
    var $commonality = 5;

    var $name = "Full Melanism";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        $dragon->shade = "Black";

        return $dragon;
    }
}

RegisterMutation(new FullMelanism());