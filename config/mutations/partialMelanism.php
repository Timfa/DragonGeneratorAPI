<?php
class PartialMelanism
{
    var $commonality = 25;

    var $name = "Partial Melanism";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        return $dragon;
    }
}

RegisterMutation(new PartialMelanism());