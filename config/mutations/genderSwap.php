<?php
class GenderSwap
{
    var $commonality = 5;

    var $name = "Gender-Swap";

    function ApplyToDragon($dragon)
    {
        $dragon->mutation->type = $this->name;

        if ($dragon->gender == "Female")
        {
            $dragon->gender = "Male";
            $dragon->clutchSizes = "0-0";
        }
        else
        {
            $dragon->gender = "Female";
        }

        return $dragon;
    }
}

RegisterMutation(new GenderSwap());