<?php
class FlightInstability
{
    var $commonality = 5;

    var $name = "Flight Instability";

    function ApplyToDragon($dragon)
    {
        $severityN = rand(0, 100);
		
		if ($severityN < 33)
		{
			$severity = "Mild ";
		}
		else if ($severityN < 66)
		{
			$severity = "";
		}
		else
		{
			$severity = "Severe ";
		}

        $dragon->geneticDefect = $severity . "Flight Instability due to shorter tail.";

        return $dragon;
    }
}

RegisterDefect(new BreathingProblems());