<?php
class MissingWing
{
    var $commonality = 5;

    var $name = "MissingWing";

    function ApplyToDragon($dragon)
    {
        if (rand(0, 100) > 50)
        {
            $dragon->wingSpan = "N/A";
            $dragon->geneticDefect = "No wings.";
        }
        else
        {
            $dragon->wingSpan = round($dragon->wingSpan / 2) . "ft";
            $dragon->geneticDefect = "Only one wing.";
        }

        return $dragon;
    }
}

RegisterDefect(new MissingWing());