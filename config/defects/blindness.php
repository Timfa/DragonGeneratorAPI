<?php
class Blindness
{
    var $commonality = 10;

    var $name = "Blindness";

    function ApplyToDragon($dragon)
    {
        $severityN = rand(0, 100);
		
		$severity = ($severityN < 50)? "Partial " : "Complete ";

        $dragon->geneticDefect = $severity . $this->name;

        return $dragon;
    }
}

RegisterDefect(new Blindness());