<?php
class MalformedJaw
{
    var $commonality = 5;

    var $name = "Malformed Jaw";

    function ApplyToDragon($dragon)
    {
        $severityN = rand(0, 100);
		
		if ($severityN < 33)
		{
			$severity = "Mild ";
		}
		else if ($severityN < 66)
		{
			$severity = "";
		}
		else
		{
			$severity = "Severe ";
		}

        $dragon->geneticDefect = $severity . $this->name;

        return $dragon;
    }
}

RegisterDefect(new MalformedJaw());