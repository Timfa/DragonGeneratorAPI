<?php
class Deafness
{
    var $commonality = 10;

    var $name = "Deafness";

    function ApplyToDragon($dragon)
    {
        $severityN = rand(0, 100);
		
		$severity = ($severityN < 50)? "Partial " : "Complete ";

        $dragon->geneticDefect = $severity . $this->name;

        return $dragon;
    }
}

RegisterDefect(new Deafness());