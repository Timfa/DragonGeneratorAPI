<?php
class AsymmetricalWings
{
    var $commonality = 5;

    var $name = "Asymmetrical Wings";

    function ApplyToDragon($dragon)
    {
        $severityN = rand(0, 100);
		
		$severity = ($severityN < 50)? "Partial " : "Complete ";

        $dragon->geneticDefect = $severity . "Flight Incapability due to asymmetrical wings.";

        return $dragon;
    }
}

RegisterDefect(new AsymmetricalWings());